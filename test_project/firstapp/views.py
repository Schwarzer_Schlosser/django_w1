from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render
from datetime import date


def hellodjango(request):
    return HttpResponse('Hello Django!')


def hellouser(request, name):
    return HttpResponse(f'Hello {name}!')


def datetoday(request):
    return HttpResponse(date.today())


def gettimeframe(request, timeframe):
    if timeframe == 'year':
        return HttpResponse(date.today().year)
    elif timeframe == 'month':
        return HttpResponse(date.today().strftime("%m"))
    elif timeframe == 'day':
        return HttpResponse(date.today().strftime("%d"))
    else:
        return HttpResponseNotFound(f'not found {timeframe}')

