from django.urls import path

from firstapp.views import hellodjango, hellouser, gettimeframe, datetoday

urlpatterns = [
    path('', hellodjango),
    path('data/', datetoday),
    path('data/<str:timeframe>/', gettimeframe),
    path('<str:name>/', hellouser),
]
